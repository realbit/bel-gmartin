﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Results : MonoBehaviour {

    public Text resultText;
    public float testTime = 50;
    public bool deletePlayerPref = false;
    
        private Score score;
    private int playerScore;

    public void Start()
    {
        score = new Score();

        if (deletePlayerPref)
            score.deleteAll();
        
        ShowResult();
    }

    private void ShowResult()
    {
        if (Data.result != Data.finish.NULL)
        {
            string msg = "";

            if (Data.result == Data.finish.LOOSE)
            {
                msg = "Has fallado la misión. Tu puntaje es 0.";
            }
            else
            {
                int minutes, seconds, position;

                minutes = (int)(Data.time / 60);
                seconds = (int)(Data.time % 60);
                playerScore = score.calculateScore(Data.time);
                //playerScore = score.calculateScore(testTime);

                msg = string.Format("Excelente trabajo soldado!\nEl tiempo en completar la misión ha sido {0:00}:{1:00} y tu puntaje es {2}", minutes, seconds, playerScore);

                position = score.newPosition();

                if (position != -1)
                {
                    GameObject.Find("PlayerName").GetComponent<InputField>().enabled = true;
                    GameObject.Find("PlayerName").GetComponent<RectTransform>().transform.position = GameObject.Find("BestName" + (position + 1).ToString()).GetComponent<RectTransform>().transform.position;

                }
            }

            showScores();

            resultText.text = msg;
        }
        else
            GameObject.Find("MsgText").GetComponent<Text>().enabled = false;

        showScores();
    }

    public void Update()
    {
        showScores();
    }

    public void setPlayerName()   
    {
        string playerName = GameObject.Find("PlayerName").GetComponent<InputField>().text;
        score.insertPlayerName(playerName);
        GameObject.Find("PlayerName").GetComponent<InputField>(). enabled = false;
        GameObject.Find("PlayerName").GetComponent<RectTransform>().transform.position = new Vector3(0, 1000, 0);
    }

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void showScores()
    {
        int i;        

        int[] scores = score.getBestScores();
        string[] names = score.getBestNames();

        for (i = 0; i < 10; i++)
        {
            GameObject.Find("BestName" + (i + 1).ToString()).GetComponent<Text>().text = names[i];
            GameObject.Find("BestScore" + (i + 1).ToString()).GetComponent<Text>().text = scores[i].ToString();
        }
    }
}
