﻿using UnityEngine;
using System.Collections;

public class Bunker : MonoBehaviour {

    public GameObject explosionPrefab;
    public GameObject gameManager;
    public float detectionRadius;
    public float fireRate = 0.5F;
    public GameObject missilePrefab;

    private GameObject missile;
    private GameObject bunker;
    private GameObject bunkerDestroyed;
    private GameManager gManager;
    private float nextFire = 0.0F;
    private Vector3 target;

    private enum states
    {
        GUARDIA, ALERTA, DESTRUIDO
    };

    private states state;

    // Use this for initialization
    void Start () {
        gManager = gameManager.GetComponent<GameManager>();

        bunker = transform.GetChild(0).gameObject;
        bunkerDestroyed = transform.GetChild(1).gameObject;
        bunker.SetActive(true);
        bunkerDestroyed.SetActive(false);
        state = states.ALERTA;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    void UpdateState()
    {
        switch (state)
        {
            case states.GUARDIA:
                UpdateDetection();
                break;

            case states.ALERTA:

                if (Time.time > nextFire)
                {
                    nextFire = Time.time + fireRate;

                    missile = (GameObject)Instantiate(missilePrefab,
                                                      new Vector3(transform.position.x,
                                                                  transform.position.y +2,
                                                                  transform.position.z),
                                                      transform.rotation);

                    missile.GetComponent<Missile>().target = target;
                    missile.tag = "MissileTank";
                }

                UpdateDetection();
                break;

            default:
                break;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Missile"))
        {
            bunkerDestroyed.SetActive(true);
            bunker.SetActive(false);
            state = states.DESTRUIDO;
            GameObject explosion = (GameObject)Instantiate(explosionPrefab,
                                                  new Vector3(transform.position.x,
                                                              transform.position.y,
                                                              transform.position.z),
                                                  transform.rotation);

            gManager.BunkerDestroyed();
        }
    }

    private void UpdateDetection()
    {
        int i = 0;
        int layerId = 8;
        int layerMask = 1 << layerId;

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRadius, layerMask);
        state = states.GUARDIA;
        while (i < hitColliders.Length)
        {
            target = hitColliders[i].transform.position;
            state = states.ALERTA;
            gManager.HeroDetected();
            i++;
        }
    }
}
