﻿using UnityEngine;
using System.Collections;

public class Level1 : MonoBehaviour, ILevel {

    private bool heroDetected = false;
    private bool enemyEliminated = false;
    private bool bunkerDestroyed = false;

    public void EnemyEliminated()
    {
        enemyEliminated = true;
    }

    public void HeroDetected()
    {
        heroDetected = true;
    }

    public void BunkerDestroyed()
    {
        bunkerDestroyed = true;
    }

    public Data.finish validateGameOver()
    {
        Data.finish result = Data.finish.NULL;

        if (enemyEliminated)
            result = Data.finish.WIN;

        return result;
    }
}
