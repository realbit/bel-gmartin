﻿using UnityEngine;
using System.Collections;

public interface ILevel {

    void EnemyEliminated();

    void HeroDetected();

    void BunkerDestroyed();

    Data.finish validateGameOver();
}
