﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour {

    public GameObject missilePrefab;
    public GameObject gameManager;
    public GameObject body;
    public GameObject bodyDead;
    public GameObject insertionPoint;
    public float speed = 1;
    public float fireRate = 0.5F;
    public bool onPause = false;
    public float launchForce = 7000f;

    private float nextFire = 0.0F;
    private GameObject missile;
   
    // Use this for initialization
    void Start()
    {
        body = transform.GetChild(0).gameObject;
        bodyDead = transform.GetChild(1).gameObject;
        body.SetActive(true);
        bodyDead.SetActive(false);

        Vector3 initialPosition = insertionPoint.gameObject.GetComponent<Transform>().position;
        transform.position = new Vector3(initialPosition.x,
                                         1.5f,
                                         initialPosition.z);
    }

	// Update is called once per frame
	void Update () {

        Vector3 newPos;

        if (Input.GetAxis("Horizontal") != 0)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                newPos = transform.right * Time.deltaTime * speed;

                if (validSceneryLimits(newPos + transform.position))
                    transform.position += newPos;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                newPos = -transform.right * Time.deltaTime * speed;

                if (validSceneryLimits(newPos + transform.position))
                    transform.position += newPos;
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            newPos = transform.forward * Time.deltaTime * speed;

            if (validSceneryLimits(newPos + transform.position))
                transform.position += newPos;
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            newPos = -transform.forward * Time.deltaTime * speed;

            if (validSceneryLimits(newPos + transform.position))
                transform.position += newPos;
        }

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            gameObject.GetComponent<AudioSource>().mute = false;
        }
        else
        {
            gameObject.GetComponent<AudioSource>().mute = true;
        }

        int layerId = 9;
        int layerMask = 1 << layerId;
        RaycastHit hit;
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

        if (!onPause)
        {
            if (Input.GetButton("Fire1") && Time.time > nextFire)
            {
                Vector3 actualPosition = transform.position;

                nextFire = Time.time + fireRate;

                Physics.Raycast(ray, out hit, Mathf.Infinity);

                missile = (GameObject)Instantiate(missilePrefab,
                                                  new Vector3(actualPosition.x,
                                                              actualPosition.y + 3,
                                                              actualPosition.z),
                                                  transform.rotation);
                missile.gameObject.GetComponent<Rigidbody>().AddForce(GameObject.Find("Main Camera").GetComponent<Transform>().forward * launchForce);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("MissileTank") || col.gameObject.CompareTag("Explosion"))
        {
            dead();
        }
    }

    private void dead()
    {
        bodyDead.SetActive(true);
        body.SetActive(false);
        gameObject.layer = 11;
        GameManager gManager = gameManager.GetComponent<GameManager>();
        gManager.HeroDied();
        Invoke("ReSpawn", 3f);
    }

    private void ReSpawn()
    {
        if (Data.lifes > 0)
        {
            body.SetActive(true);
            bodyDead.SetActive(false);
            bodyDead.transform.position = transform.position;
            bodyDead.transform.rotation = transform.rotation;
            gameObject.layer = 8;
        }
    }

    private bool validSceneryLimits(Vector3 position)
    {
        bool result = false;

        if ((position.x >= -249 && position.x <= 249) && (position.z >= -249 && position.z <= 249))
            result = true;

        return result;
    }
}
