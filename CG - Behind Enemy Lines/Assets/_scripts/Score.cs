﻿using UnityEngine;
using System.Collections;

public class Score {

    private int[] bestScores = new int[10];
    private string[] bestNames = new string[10];
    private int position = -1;

    public Score()
    {
        loadBestScores();
    }

    public int calculateScore(float gameTime)
    {
        int i, j;
        int score = 1000;
        int time = (int)gameTime;
        
        score = score - time * 2 + Data.lifes * 5;

        for (i = 0; i < 10; i++)
        {
            if (score > bestScores[i])
            {
                for (j = 9; j > i; j--)
                {
                    bestScores[j] = bestScores[j - 1];
                    bestNames[j] = bestNames[j - 1];
                }

                bestScores[i] = score;
                position = i;
                updateScores();
                break;
            }
        }        

        return score;
    }

    private void loadBestScores()
    {
        int i;

        for(i=0; i<10; i++)
        {
            if (PlayerPrefs.HasKey("Score" + i.ToString()))
            {
                bestScores[i] = PlayerPrefs.GetInt("Score" + i.ToString());
                bestNames[i] = PlayerPrefs.GetString("Name" + i.ToString());
            }
            else
            {
                bestScores[i] = 0;
                bestNames[i] = "-";
            }
        }
    }

    private void updateScores()
    {
        int i;

        for(i=0; i<10; i++)
        {
            PlayerPrefs.SetInt("Score" + i.ToString(), bestScores[i]);
            PlayerPrefs.SetString("Name" + i.ToString(), bestNames[i]);           
        }
    }

    public void insertPlayerName(string playerName)
    {
        bestNames[position] = playerName;
        updateScores();
    }

    public int[] getBestScores()
    {
        return bestScores;
    }

    public string[] getBestNames()
    {
        return bestNames;
    }

    public int newPosition()
    {
        return position;
    }

    public void deleteAll()
    {
        PlayerPrefs.DeleteAll();
        loadBestScores();
    }
}
