﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour {

    public void startScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
