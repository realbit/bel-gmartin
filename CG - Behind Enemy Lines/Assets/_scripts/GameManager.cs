﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public int lifes = 3;
    public Text lifeLabel;
    public Text enemyLabel;
    public Text bunkerLabel;
    public Text resultLabel;
    public Text timeLabel;
    public GameObject enemyPrefab;
    public GameObject[] waypoints;
    public GameObject[] bunkers;
    public GameObject[] pauseMenuObjects;
    public GameObject level;

    private GameObject enemy;
    private ILevel levelScript;
    private GameObject smokeEP;
    private float time = 0f;
    private string textTime;
    private int enemyLeft;
    private int bunkerLeft;
    private bool onPause = false;
    private bool onMute = false;
    private bool isGameOver = false;


    // Use this for initialization
    void Start()
    {
        //Hide Menu Pause elements.
        hidePaused();
        onPause = false;
        onMute = false;

        Data.gameManager = this.gameObject;

        time = Time.time;

        smokeEP = GameObject.Find("SmokeExtractionPoint");
        smokeEP.SetActive(false);

        levelScript = level.GetComponent<ILevel>();

        if (bunkers.Length > 0)
        {
            bunkerLeft = bunkers.Length;
        }
        else
        {
            bunkerLabel.text = "";
        }

        //Se instancian los enemigos de acuerdo a la cantidad de grupos de waypoints definidos
        if (waypoints != null && waypoints.Length > 0)
        {
            for (int i = 0; i < waypoints.Length; i++)
            {
                enemyLeft = waypoints.Length;

                enemy = (GameObject)Instantiate(enemyPrefab,
                                            new Vector3(0, 5, 0),
                                            new Quaternion(0, 0, 0, 0));

                enemy.gameObject.GetComponent<Tank>().gameManager = this.gameObject;

                Transform[] wps = waypoints[i].GetComponentsInChildren<Transform>();
                IList wpList = new ArrayList();
                Vector3 wp;

                for (int j = 0; j < wps.Length; j++)
                {
                    if (!wps[j].gameObject.CompareTag("WaypointGroup"))
                    {
                        wp = wps[j].position;
                        wpList.Add(wp);

                        //Se desactiva el renderizado del waypoint
                        wps[j].GetComponent<Renderer>().enabled = false;
                    }
                }
                enemy.gameObject.GetComponent<Transform>().position = (Vector3)wpList[0];
                enemy.gameObject.GetComponent<Tank>().WayPointsList = wpList;               
            }
        }

        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isGameOver)
        {
            switch (levelScript.validateGameOver())
            {
                case Data.finish.LOOSE:
                    Data.result = Data.finish.LOOSE;
                    resultLabel.text = "MISION FALLIDA...";
                    GameOver();
                    break;

                case Data.finish.WIN:
                    Data.result = Data.finish.WIN;
                    Data.time = time;
                    resultLabel.text = "MISION CUMPLIDA SOLDADO, DIRIJASE AL PUNTO DE EXTRACCION.";
                    smokeEP.SetActive(true);
                    break;

                case Data.finish.NULL:
                    break;
            }

            time = Time.time - time;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseMenu();
        }

        UpdateUI();

        if (!onPause && !onMute)
            AudioListener.pause = false;
        else
            AudioListener.pause = true;
    }

    protected void UpdateUI()
    {
        lifeLabel.text = "Vidas: " + lifes.ToString();
        enemyLabel.text = "Tanques: " + enemyLeft.ToString();

        if (bunkers.Length > 0)
            bunkerLabel.text = "Bunkers: " + bunkerLeft.ToString();

        int minutes, seconds;
        minutes = (int)(time / 60);
        seconds = (int)(time % 60);
        
        textTime = string.Format("{0:00}:{1:00}", minutes, seconds);
        timeLabel.text = "Tiempo:" + textTime;
    }

    public void HeroDied()
    {
        if (lifes > 0)
            lifes--;

        Data.lifes = lifes;

        if(lifes == 0)
        {
            Data.result = Data.finish.LOOSE;
            resultLabel.text = "MISION FALLIDA...";
            GameOver();
        }
            

        UpdateUI();
    }

    public void EnemyDestroyed()
    {
        if (enemyLeft > 0)
            enemyLeft--;

        if (enemyLeft == 0)
            levelScript.EnemyEliminated();

        UpdateUI();
    }
    
    public void BunkerDestroyed()
    {
        
        if (bunkerLeft > 0)
            bunkerLeft--;

        Debug.Log(bunkerLeft);

        if (bunkerLeft == 0)
            levelScript.BunkerDestroyed();
    }

    public void HeroDetected()
    {
        levelScript.HeroDetected();
    }

    public void ExtractionPoint()
    {
        if (Data.result == Data.finish.WIN)
        {
            resultLabel.text = "MISION COMPLETADA!!!";
            UpdateUI();
            GameOver();
        }
    }

    public void GameOver()
    {
        isGameOver = true;
        Invoke("GoToResults", 4f);
        UpdateUI();
    }    

    private void GoToResults()
    {
        SceneManager.LoadScene("results");
    }

    public void PauseMenu()
    {
        if (onPause)
        {
            Time.timeScale = 1f;
            GameObject.Find("Main Camera").GetComponent<MouseAimCamera>().onPause = false;
            GameObject.Find("Player").GetComponent<Hero>().onPause = false;
            hidePaused();
        }
        else
        {
            Time.timeScale = 0f;
            GameObject.Find("Main Camera").GetComponent<MouseAimCamera>().onPause = true;
            GameObject.Find("Player").GetComponent<Hero>().onPause = true;
            showPaused();
        }
            
        onPause = !onPause;
        
    }

    public void hidePaused()
    {
        foreach (GameObject g in pauseMenuObjects)
        {
            g.SetActive(false);
        }
    }

    public void showPaused()
    {
        foreach (GameObject g in pauseMenuObjects)
        {
            g.SetActive(true);
        }
    }

    public void goToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("mainMenu");
    }

    public void mute()
    {
        string msg;

        if (!onMute)
            msg = "Play Sounds";
        else
            msg = "Mute Sounds";

        GameObject.Find("btnSound").GetComponent<Button>().GetComponentInChildren<Text>().text = msg;
        onMute = !onMute;
    }
}
