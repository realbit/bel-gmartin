﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour {

    public GameObject explosionPrefab;

    // Use this for initialization
    void Start () {
        Invoke("DestroyMe", 3f);
    }

    private void DestroyMe()
    {
        GetComponent<Renderer>().enabled = false;
        GameObject explosion = (GameObject)Instantiate(explosionPrefab,
                                                  new Vector3(transform.position.x,
                                                              transform.position.y,
                                                              transform.position.z),
                                                  transform.rotation);

        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Ground") || col.gameObject.CompareTag("Bunker"))
        {
            gameObject.SetActive(false);
            GetComponent<Collider>().enabled = false;
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Tank") || col.gameObject.CompareTag("Bunker"))
        {
            gameObject.SetActive(false);
            GetComponent<Collider>().enabled = false;
            Destroy(gameObject);
        }
    }
}
