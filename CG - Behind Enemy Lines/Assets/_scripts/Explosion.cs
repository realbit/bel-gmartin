﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

    private GameObject _particleEffect;
    public float radius = 50.0F;
    public float power = 10000.0F;

    // Use this for initialization
    void Start () {
        int layerMissile = 10;
        int layerMask = 1 << layerMissile;

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius, ~layerMask);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 10F);
        }
        
        Invoke("EndEffect", 1f);
    }

    private void EndEffect()
    {
        GetComponent<Collider>().enabled = false;
        Invoke("DestroyMe", 2f);
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}
