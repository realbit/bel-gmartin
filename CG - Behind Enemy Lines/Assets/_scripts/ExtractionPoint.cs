﻿using UnityEngine;
using System.Collections;

public class ExtractionPoint : MonoBehaviour {

    public GameObject gameManager;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Hero"))
        {
            GameManager gManager = gameManager.GetComponent<GameManager>();
            gManager.ExtractionPoint();

        }
    }
}
