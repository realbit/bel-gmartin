﻿using UnityEngine;
using System.Collections;

public class MouseAimCamera : MonoBehaviour {

    public GameObject target;
    public float rotateSpeed = 5;
    public bool onPause = false;
    
    private Vector3 offset;
    private Rect crossHairRect;
    private Texture crossHairTexture;

    void Start()
    {
        float crossHairSize = Screen.width * 0.05f;
        crossHairTexture = Resources.Load("Textures/crosshair") as Texture;
        crossHairRect = new Rect(Screen.width / 2 - crossHairSize / 2,
                                 Screen.height / 2 - crossHairSize / 2,
                                 crossHairSize,
                                 crossHairSize);

        offset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
        if (!onPause)
        {
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
                target.transform.Rotate(0, horizontal, 0);

                float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
                transform.Rotate(-vertical, 0, 0);

                float desiredAngleY = target.transform.eulerAngles.y;
                Quaternion rotation = Quaternion.Euler(0, desiredAngleY, 0);
                transform.position = target.transform.position - (rotation * offset);
            }
        }
    }

    void OnGUI()
    {
        if(!onPause)
            GUI.DrawTexture(crossHairRect, crossHairTexture);
    }
}
