﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {

    public float speed;
    public Vector3 target;
    public GameObject explosionPrefab;

    //private GameObject particleEffect;

    // Use this for initialization
    void Start () {
        Invoke("DestroyMe", 3f);
    }
	
	// Update is called once per frame
	void Update () {
        MoveToTarget();
	}

    private void MoveToTarget()
    {
        //Obtengo la posición actual
        Vector3 actualPosition = transform.position;
        transform.LookAt(target);

        if (!actualPosition.Equals(target))
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(actualPosition, target, step);
        }
        else
        {
            DestroyMe();
        }        
    }

    private void DestroyMe()
    {
        GetComponent<Renderer>().enabled = false;
        GameObject explosion = (GameObject)Instantiate(explosionPrefab,
                                                  new Vector3(transform.position.x,
                                                              transform.position.y,
                                                              transform.position.z),
                                                  transform.rotation);
        
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Hero"))
        {
            DestroyMe();
        }
    }
}
