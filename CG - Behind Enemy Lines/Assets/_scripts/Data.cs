﻿using UnityEngine;
using System.Collections;

public static class Data{

    public enum finish
    {
        WIN, LOOSE, NULL
    }

    public static float time;
    public static int lifes;
    public static finish result = finish.NULL;
    public static GameObject gameManager;

}
