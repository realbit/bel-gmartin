﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public Text missionText;

    private string mission;
    private string levelScene;

    public void Start()
    {
        mission = "-- CLASIFICADO -- \n\nSoldado:\n\nSu misión consiste en eliminar toda presencia de enemigo en la zona.\nAl finalizar diríjase al punto de extracción.\n\nGeneral Duke N.\n\n-- CLASIFICADO --";
        levelScene = "level1";
        UpdateUI();
    }

    public void Update()
    {
        UpdateUI();
    }

    public void LauncMission()
    {
        SceneManager.LoadScene(levelScene);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void levelSelected()
    {
        int level = GameObject.Find("ddlLeverSelector").GetComponent<Dropdown>().value;

        if (level == 0)
        {
            mission = "-- CLASIFICADO -- \n\nSoldado:\n\nSu misión consiste en eliminar toda presencia de enemigo en la zona.\nAl finalizar diríjase al punto de extracción.\n\nGeneral Duke N.\n\n-- CLASIFICADO --";
            levelScene = "level1";
        }
        else if (level == 1)
        {
            mission = "-- CLASIFICADO -- \n\nSoldado:\n\nEn el cumplimiento con su deber, deberá destruir todos los bunkers que el enemigo tiene en la zona de conflicto.\nAl finalizar diríjase al punto de extracción. Buena suerte!!!\n\nGeneral Duke N.\n\n-- CLASIFICADO --";
            levelScene = "level2";
        }
        else
        {
            mission = "-- CLASIFICADO -- \n\nSoldado:\n\nEsta misión será dificil. Debe eliminar furtivamente el bunquer del enemigo. Estrictamente NO DEBE SER DETECTADO, de lo contrario la misión habrá fallado.\nBuena suerte!!!\n\nGeneral Duke N.\n-- CLASIFICADO --";
            levelScene = "level3";
        }

        UpdateUI();
    }

    private void UpdateUI()
    {
        missionText.text = mission;
    }
}
