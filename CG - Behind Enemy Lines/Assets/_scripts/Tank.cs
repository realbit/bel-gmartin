﻿using UnityEngine;
using System.Collections;

public class Tank : MonoBehaviour {

    public IList WayPointsList;
    public float speed;
    public float detectionRadius;
    public float fireRate = 0.5F;
    public GameObject missilePrefab;
    public GameObject explosionPrefab;
    public GameObject gameManager;

    private float nextFire = 0.0F;
    private Vector3 currentWayPoint;
    private Vector3 target;
    private int currentIndexWP;
    private GameObject missile;
    private Vector3[] wayPoints;
    private GameManager gManager;
    private GameObject tank;
    private GameObject tankDestroyed;

    private enum states
    {
        SPAWN, GUARDIA, ALERTA, DESTRUIDO
    };

    private Tank.states state;

	// Use this for initialization
	void Start () {
        //Inicializacion de estado
        state = states.SPAWN;
        gManager = gameManager.GetComponent<GameManager>();
    }
    
	// Update is called once per frame
	void Update () {
        UpdateState();
    }

    void UpdateState()
    {
        switch (state)
        {
            case states.SPAWN:
                /*El estado SPAWN solo inicializa el enemigo luego de instanciarse.*/

                tank = transform.GetChild(0).gameObject;
                tankDestroyed = transform.GetChild(1).gameObject;
                tank.SetActive(true);
                tankDestroyed.SetActive(false);

                if (WayPointsList != null)
                    currentWayPoint = (Vector3)WayPointsList[0];
                else
                    currentWayPoint = new Vector3(0, 0, 0);

                currentIndexWP = 0;
                state = states.GUARDIA;
                
                break;
            case states.GUARDIA:
                
                MoveToWayPoint();
                UpdateDetection();
                
                break;
            case states.ALERTA:
                
                if (Time.time > nextFire) {
                    nextFire = Time.time + fireRate;
                    
                    missile = (GameObject)Instantiate(missilePrefab,
                                                      new Vector3(transform.position.x,
                                                                  transform.position.y + 5,
                                                                  transform.position.z),
                                                      transform.rotation);

                    missile.GetComponent<Missile>().target = target;
                    missile.tag = "MissileTank";
                }

                MoveToWayPoint();
                UpdateDetection();
                break;

            default:
                break;
        }
    }

    private void UpdateDetection()
    {
        int i = 0;
        int layerId = 8;
        int layerMask = 1 << layerId;

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRadius, layerMask);
        state = states.GUARDIA;
        while (i < hitColliders.Length)
        {
            target = hitColliders[i].transform.position;
            state = states.ALERTA;
            gManager.HeroDetected();
            i++;
        }
    }

    private void MoveToWayPoint()
    {
        //Obtengo la posición actual

        if (WayPointsList != null)
        {
            Vector3 actualPosition = transform.position;


            if (!actualPosition.Equals(currentWayPoint))
            {
                /*Si la posición actual no corresponde con la posción del waypoint actual,
                  se procede a que el tanque enemigo apunte hacia el waypoint y se traslade hacia él.*/
                float step = speed * Time.deltaTime;
                transform.LookAt(currentWayPoint);
                transform.position = Vector3.MoveTowards(actualPosition, currentWayPoint, step);
            }
            else
            {
                /*Si la posición actual coincide con el waypoint actual, se cambia por el próximo en la lista.
                  Si el waypoint es el último de la lista se setea el primero.*/
                currentIndexWP++;

                if (currentIndexWP == WayPointsList.Count)
                {
                    currentIndexWP = 0;
                }

                currentWayPoint = (Vector3)WayPointsList[currentIndexWP];

            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Missile"))
        {
            state = states.DESTRUIDO;
            tankDestroyed.SetActive(true);
            tank.SetActive(false);
            gameObject.GetComponent<AudioSource>().enabled = false;
            GetComponent<Collider>().enabled = false;
            GameObject explosion = (GameObject)Instantiate(explosionPrefab,
                                                  new Vector3(transform.position.x,
                                                              transform.position.y,
                                                              transform.position.z),
                                                  transform.rotation);

            
            gManager.EnemyDestroyed();
        }
    }
}

